﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Windows.Forms;

using Newtonsoft.Json;
using Inventory_Management_Program.utils;

namespace Inventory_Management_Program
{
    public partial class FormLogin : Form
    {
        private static readonly HttpClient httpClient = new HttpClient();

        public FormLogin()
        {
            InitializeComponent();

            Text = Application.ProductName + " - Login";
        }

        private async void btnLogin_ClickAsync(object sender, EventArgs e)
        {
            string username = TbUsername.Text;
            string password = TbPassword.Text;
            string branch = (ComboBranch.SelectedItem == null) ? "" : ComboBranch.SelectedItem.ToString();

            if (username.Length > 0 && password.Length > 0 && branch.Length > 0)
            {
                BtnLogin.Enabled = false;

                // Authentication
                var parameters = new Dictionary<string, string>
                {
                    {"username", username},
                    {"password", password}
                };

                var content = new FormUrlEncodedContent(parameters);
                var response = await httpClient.PostAsync(Constants.LoginURL, content);
                string responseString = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseString);

                BtnLogin.Enabled = true;

                if (msg != null)
                {
                    if (msg.code == 0)
                    {
                        Hide();
                        new FormMain(branch).Show();
                    }
                    else
                    {
                        MessageBox.Show(this, msg.message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                MessageBox.Show(this, "One or more required fields are left empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            TbUsername.ResetText();
            TbPassword.ResetText();
            ComboBranch.ResetText();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {

            if (!NetUtility.HasConnectionToServer())
            {
                var result = MessageBox.Show(this, "Server is unreachable", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

                if (result == DialogResult.OK)
                {
                    Application.Exit();
                }
            }
        }
    }
}
