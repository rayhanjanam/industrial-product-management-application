﻿namespace Inventory_Management_Program
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label13;
            this.TabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageSearch = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.TbSearch = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.BtnSearch = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.ListSearch = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tabPagePurchase = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnPurchase = new System.Windows.Forms.Button();
            this.datePicker = new System.Windows.Forms.DateTimePicker();
            this.tbQty = new System.Windows.Forms.TextBox();
            this.tbPrice = new System.Windows.Forms.TextBox();
            this.tbProductName = new System.Windows.Forms.TextBox();
            this.tabPageSales = new System.Windows.Forms.TabPage();
            this.LblProductName = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnRefresh = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.comboProductName = new System.Windows.Forms.ComboBox();
            this.btnClearSale = new System.Windows.Forms.Button();
            this.btnSupply = new System.Windows.Forms.Button();
            this.datePickerSale = new System.Windows.Forms.DateTimePicker();
            this.tbLeadTime = new System.Windows.Forms.TextBox();
            this.tbQtySale = new System.Windows.Forms.TextBox();
            this.tbPriceSale = new System.Windows.Forms.TextBox();
            this.tabPageClientData = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LblID = new System.Windows.Forms.Label();
            this.TbName = new System.Windows.Forms.TextBox();
            this.BtnReset = new System.Windows.Forms.Button();
            this.TbAddress = new System.Windows.Forms.TextBox();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.BtnUpdate = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.BtnSave = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.TbPhone = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.ComboType = new System.Windows.Forms.ComboBox();
            this.TbEmail = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ListClient = new System.Windows.Forms.ListView();
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel6 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label13 = new System.Windows.Forms.Label();
            this.TabControlMain.SuspendLayout();
            this.tabPageSearch.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabPagePurchase.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPageSales.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPageClientData.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label1.ForeColor = System.Drawing.Color.White;
            label1.Location = new System.Drawing.Point(331, 11);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(221, 37);
            label1.TabIndex = 0;
            label1.Text = "Product Purchase";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.ForeColor = System.Drawing.Color.White;
            label2.Location = new System.Drawing.Point(281, 48);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(315, 17);
            label2.TabIndex = 1;
            label2.Text = "Enter the product name, purchase price and quantity";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label3.Location = new System.Drawing.Point(167, 142);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(112, 20);
            label3.TabIndex = 1;
            label3.Text = "Product Name:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label4.Location = new System.Drawing.Point(173, 178);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(106, 20);
            label4.TabIndex = 2;
            label4.Text = "Purchase Price:";
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label5.Location = new System.Drawing.Point(211, 214);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(68, 20);
            label5.TabIndex = 3;
            label5.Text = "Quantity:";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(235, 250);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(44, 20);
            label6.TabIndex = 4;
            label6.Text = "Date:";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label7.Location = new System.Drawing.Point(252, 341);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(44, 20);
            label7.TabIndex = 10;
            label7.Text = "Date:";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label8.Location = new System.Drawing.Point(228, 273);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(68, 20);
            label8.TabIndex = 9;
            label8.Text = "Quantity:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label9.Location = new System.Drawing.Point(252, 239);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(44, 20);
            label9.TabIndex = 8;
            label9.Text = "Price:";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label10.Location = new System.Drawing.Point(184, 142);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(111, 20);
            label10.TabIndex = 6;
            label10.Text = "Select Product:";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.ForeColor = System.Drawing.Color.White;
            label11.Location = new System.Drawing.Point(250, 48);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(376, 17);
            label11.TabIndex = 7;
            label11.Text = "Enter the product name, purchase price, quantity and lead time";
            // 
            // label12
            // 
            label12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            label12.AutoSize = true;
            label12.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label12.ForeColor = System.Drawing.Color.White;
            label12.Location = new System.Drawing.Point(350, 11);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(176, 37);
            label12.TabIndex = 5;
            label12.Text = "Product Sales";
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label13.Location = new System.Drawing.Point(215, 307);
            label13.Name = "label13";
            label13.Size = new System.Drawing.Size(81, 20);
            label13.TabIndex = 11;
            label13.Text = "Lead Time:";
            // 
            // TabControlMain
            // 
            this.TabControlMain.Controls.Add(this.tabPageSearch);
            this.TabControlMain.Controls.Add(this.tabPagePurchase);
            this.TabControlMain.Controls.Add(this.tabPageSales);
            this.TabControlMain.Controls.Add(this.tabPageClientData);
            this.TabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControlMain.HotTrack = true;
            this.TabControlMain.ItemSize = new System.Drawing.Size(120, 22);
            this.TabControlMain.Location = new System.Drawing.Point(0, 0);
            this.TabControlMain.Margin = new System.Windows.Forms.Padding(0);
            this.TabControlMain.Name = "TabControlMain";
            this.TabControlMain.Padding = new System.Drawing.Point(0, 0);
            this.TabControlMain.SelectedIndex = 0;
            this.TabControlMain.Size = new System.Drawing.Size(884, 531);
            this.TabControlMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.TabControlMain.TabIndex = 0;
            // 
            // tabPageSearch
            // 
            this.tabPageSearch.Controls.Add(this.panel5);
            this.tabPageSearch.Controls.Add(this.panel4);
            this.tabPageSearch.Controls.Add(this.panel3);
            this.tabPageSearch.Location = new System.Drawing.Point(4, 26);
            this.tabPageSearch.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageSearch.Name = "tabPageSearch";
            this.tabPageSearch.Size = new System.Drawing.Size(876, 501);
            this.tabPageSearch.TabIndex = 2;
            this.tabPageSearch.Text = "  SEARCH  ";
            this.tabPageSearch.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.TbSearch);
            this.panel5.Controls.Add(this.label17);
            this.panel5.Controls.Add(this.BtnSearch);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 88);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(876, 56);
            this.panel5.TabIndex = 7;
            // 
            // TbSearch
            // 
            this.TbSearch.Location = new System.Drawing.Point(84, 15);
            this.TbSearch.Name = "TbSearch";
            this.TbSearch.Size = new System.Drawing.Size(653, 25);
            this.TbSearch.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(71, 17);
            this.label17.TabIndex = 1;
            this.label17.Text = "Search for:";
            // 
            // BtnSearch
            // 
            this.BtnSearch.Location = new System.Drawing.Point(742, 14);
            this.BtnSearch.Name = "BtnSearch";
            this.BtnSearch.Size = new System.Drawing.Size(122, 27);
            this.BtnSearch.TabIndex = 3;
            this.BtnSearch.Text = "Search";
            this.BtnSearch.UseVisualStyleBackColor = true;
            this.BtnSearch.Click += new System.EventHandler(this.BtnSearch_ClickAsync);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label18);
            this.panel4.Controls.Add(this.ListSearch);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 88);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(876, 413);
            this.panel4.TabIndex = 6;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(8, 59);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(92, 17);
            this.label18.TabIndex = 5;
            this.label18.Text = "Search results:";
            // 
            // ListSearch
            // 
            this.ListSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ListSearch.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader7,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6});
            this.ListSearch.FullRowSelect = true;
            this.ListSearch.GridLines = true;
            this.ListSearch.Location = new System.Drawing.Point(10, 79);
            this.ListSearch.MultiSelect = false;
            this.ListSearch.Name = "ListSearch";
            this.ListSearch.Size = new System.Drawing.Size(854, 323);
            this.ListSearch.TabIndex = 4;
            this.ListSearch.UseCompatibleStateImageBehavior = false;
            this.ListSearch.View = System.Windows.Forms.View.Details;
            this.ListSearch.DoubleClick += new System.EventHandler(this.ListSearch_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "ID";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Product Name";
            this.columnHeader2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader2.Width = 100;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Batch";
            this.columnHeader3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader3.Width = 100;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Units";
            this.columnHeader7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader7.Width = 100;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Purchase Price";
            this.columnHeader4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader4.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Date";
            this.columnHeader5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader5.Width = 100;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Branch";
            this.columnHeader6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.columnHeader6.Width = 100;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.SteelBlue;
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.ForeColor = System.Drawing.Color.White;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(876, 88);
            this.panel3.TabIndex = 0;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(344, 48);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(189, 17);
            this.label16.TabIndex = 1;
            this.label16.Text = "Search for purchased products";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(341, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(194, 37);
            this.label15.TabIndex = 0;
            this.label15.Text = "Product Search";
            // 
            // tabPagePurchase
            // 
            this.tabPagePurchase.Controls.Add(this.panel1);
            this.tabPagePurchase.Controls.Add(this.btnClear);
            this.tabPagePurchase.Controls.Add(this.btnPurchase);
            this.tabPagePurchase.Controls.Add(this.datePicker);
            this.tabPagePurchase.Controls.Add(this.tbQty);
            this.tabPagePurchase.Controls.Add(this.tbPrice);
            this.tabPagePurchase.Controls.Add(this.tbProductName);
            this.tabPagePurchase.Controls.Add(label6);
            this.tabPagePurchase.Controls.Add(label5);
            this.tabPagePurchase.Controls.Add(label4);
            this.tabPagePurchase.Controls.Add(label3);
            this.tabPagePurchase.Location = new System.Drawing.Point(4, 26);
            this.tabPagePurchase.Margin = new System.Windows.Forms.Padding(0);
            this.tabPagePurchase.Name = "tabPagePurchase";
            this.tabPagePurchase.Size = new System.Drawing.Size(876, 501);
            this.tabPagePurchase.TabIndex = 0;
            this.tabPagePurchase.Text = "  PURCHASE  ";
            this.tabPagePurchase.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.SteelBlue;
            this.panel1.Controls.Add(label1);
            this.panel1.Controls.Add(label2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(876, 88);
            this.panel1.TabIndex = 11;
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(445, 308);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(119, 32);
            this.btnClear.TabIndex = 10;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnPurchase
            // 
            this.btnPurchase.Location = new System.Drawing.Point(307, 308);
            this.btnPurchase.Name = "btnPurchase";
            this.btnPurchase.Size = new System.Drawing.Size(119, 32);
            this.btnPurchase.TabIndex = 9;
            this.btnPurchase.Text = "Purchase";
            this.btnPurchase.UseVisualStyleBackColor = true;
            this.btnPurchase.Click += new System.EventHandler(this.btnPurchase_ClickAsync);
            // 
            // datePicker
            // 
            this.datePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePicker.Location = new System.Drawing.Point(307, 254);
            this.datePicker.Name = "datePicker";
            this.datePicker.Size = new System.Drawing.Size(257, 25);
            this.datePicker.TabIndex = 8;
            // 
            // tbQty
            // 
            this.tbQty.Location = new System.Drawing.Point(307, 217);
            this.tbQty.Name = "tbQty";
            this.tbQty.Size = new System.Drawing.Size(257, 25);
            this.tbQty.TabIndex = 7;
            // 
            // tbPrice
            // 
            this.tbPrice.Location = new System.Drawing.Point(307, 181);
            this.tbPrice.Name = "tbPrice";
            this.tbPrice.Size = new System.Drawing.Size(257, 25);
            this.tbPrice.TabIndex = 6;
            // 
            // tbProductName
            // 
            this.tbProductName.Location = new System.Drawing.Point(307, 145);
            this.tbProductName.Name = "tbProductName";
            this.tbProductName.Size = new System.Drawing.Size(359, 25);
            this.tbProductName.TabIndex = 5;
            // 
            // tabPageSales
            // 
            this.tabPageSales.Controls.Add(this.LblProductName);
            this.tabPageSales.Controls.Add(this.label14);
            this.tabPageSales.Controls.Add(this.BtnRefresh);
            this.tabPageSales.Controls.Add(this.panel2);
            this.tabPageSales.Controls.Add(this.comboProductName);
            this.tabPageSales.Controls.Add(this.btnClearSale);
            this.tabPageSales.Controls.Add(this.btnSupply);
            this.tabPageSales.Controls.Add(this.datePickerSale);
            this.tabPageSales.Controls.Add(this.tbLeadTime);
            this.tabPageSales.Controls.Add(this.tbQtySale);
            this.tabPageSales.Controls.Add(this.tbPriceSale);
            this.tabPageSales.Controls.Add(label13);
            this.tabPageSales.Controls.Add(label7);
            this.tabPageSales.Controls.Add(label8);
            this.tabPageSales.Controls.Add(label9);
            this.tabPageSales.Controls.Add(label10);
            this.tabPageSales.Location = new System.Drawing.Point(4, 26);
            this.tabPageSales.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageSales.Name = "tabPageSales";
            this.tabPageSales.Size = new System.Drawing.Size(876, 501);
            this.tabPageSales.TabIndex = 1;
            this.tabPageSales.Text = "  SALES  ";
            this.tabPageSales.UseVisualStyleBackColor = true;
            // 
            // LblProductName
            // 
            this.LblProductName.AutoSize = true;
            this.LblProductName.Font = new System.Drawing.Font("Segoe UI Semibold", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblProductName.ForeColor = System.Drawing.SystemColors.GrayText;
            this.LblProductName.Location = new System.Drawing.Point(319, 205);
            this.LblProductName.Name = "LblProductName";
            this.LblProductName.Size = new System.Drawing.Size(94, 20);
            this.LblProductName.TabIndex = 23;
            this.LblProductName.Text = "Not selected";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(189, 203);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(107, 20);
            this.label14.TabIndex = 22;
            this.label14.Text = "Product Name:";
            // 
            // BtnRefresh
            // 
            this.BtnRefresh.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnRefresh.Location = new System.Drawing.Point(593, 145);
            this.BtnRefresh.Name = "BtnRefresh";
            this.BtnRefresh.Size = new System.Drawing.Size(75, 25);
            this.BtnRefresh.TabIndex = 21;
            this.BtnRefresh.Text = "Refresh";
            this.BtnRefresh.UseVisualStyleBackColor = true;
            this.BtnRefresh.Click += new System.EventHandler(this.BtnRefresh_ClickAsync);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.SteelBlue;
            this.panel2.Controls.Add(label11);
            this.panel2.Controls.Add(label12);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.Color.White;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(876, 88);
            this.panel2.TabIndex = 20;
            // 
            // comboProductName
            // 
            this.comboProductName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboProductName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboProductName.FormattingEnabled = true;
            this.comboProductName.Location = new System.Drawing.Point(321, 145);
            this.comboProductName.Name = "comboProductName";
            this.comboProductName.Size = new System.Drawing.Size(257, 25);
            this.comboProductName.Sorted = true;
            this.comboProductName.TabIndex = 19;
            this.comboProductName.SelectedIndexChanged += new System.EventHandler(this.comboProductName_SelectedIndexChanged);
            // 
            // btnClearSale
            // 
            this.btnClearSale.Location = new System.Drawing.Point(460, 386);
            this.btnClearSale.Name = "btnClearSale";
            this.btnClearSale.Size = new System.Drawing.Size(118, 31);
            this.btnClearSale.TabIndex = 18;
            this.btnClearSale.Text = "Clear";
            this.btnClearSale.UseVisualStyleBackColor = true;
            this.btnClearSale.Click += new System.EventHandler(this.btnClearSale_Click);
            // 
            // btnSupply
            // 
            this.btnSupply.Location = new System.Drawing.Point(321, 386);
            this.btnSupply.Name = "btnSupply";
            this.btnSupply.Size = new System.Drawing.Size(119, 31);
            this.btnSupply.TabIndex = 17;
            this.btnSupply.Text = "Supply";
            this.btnSupply.UseVisualStyleBackColor = true;
            this.btnSupply.Click += new System.EventHandler(this.btnSupply_ClickAsync);
            // 
            // datePickerSale
            // 
            this.datePickerSale.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.datePickerSale.Location = new System.Drawing.Point(321, 341);
            this.datePickerSale.Name = "datePickerSale";
            this.datePickerSale.Size = new System.Drawing.Size(257, 25);
            this.datePickerSale.TabIndex = 16;
            // 
            // tbLeadTime
            // 
            this.tbLeadTime.Location = new System.Drawing.Point(321, 306);
            this.tbLeadTime.Name = "tbLeadTime";
            this.tbLeadTime.Size = new System.Drawing.Size(257, 25);
            this.tbLeadTime.TabIndex = 15;
            // 
            // tbQtySale
            // 
            this.tbQtySale.Location = new System.Drawing.Point(321, 272);
            this.tbQtySale.Name = "tbQtySale";
            this.tbQtySale.Size = new System.Drawing.Size(257, 25);
            this.tbQtySale.TabIndex = 14;
            // 
            // tbPriceSale
            // 
            this.tbPriceSale.Location = new System.Drawing.Point(322, 238);
            this.tbPriceSale.Name = "tbPriceSale";
            this.tbPriceSale.Size = new System.Drawing.Size(256, 25);
            this.tbPriceSale.TabIndex = 13;
            // 
            // tabPageClientData
            // 
            this.tabPageClientData.Controls.Add(this.groupBox2);
            this.tabPageClientData.Controls.Add(this.groupBox1);
            this.tabPageClientData.Controls.Add(this.panel6);
            this.tabPageClientData.Location = new System.Drawing.Point(4, 26);
            this.tabPageClientData.Margin = new System.Windows.Forms.Padding(0);
            this.tabPageClientData.Name = "tabPageClientData";
            this.tabPageClientData.Size = new System.Drawing.Size(876, 501);
            this.tabPageClientData.TabIndex = 3;
            this.tabPageClientData.Text = "  CLIENT DATA  ";
            this.tabPageClientData.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LblID);
            this.groupBox2.Controls.Add(this.TbName);
            this.groupBox2.Controls.Add(this.BtnReset);
            this.groupBox2.Controls.Add(this.TbAddress);
            this.groupBox2.Controls.Add(this.BtnDelete);
            this.groupBox2.Controls.Add(this.label21);
            this.groupBox2.Controls.Add(this.BtnUpdate);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.BtnSave);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.TbPhone);
            this.groupBox2.Controls.Add(this.label26);
            this.groupBox2.Controls.Add(this.label28);
            this.groupBox2.Controls.Add(this.ComboType);
            this.groupBox2.Controls.Add(this.TbEmail);
            this.groupBox2.Controls.Add(this.label27);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 88);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(876, 155);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Edit client data";
            // 
            // LblID
            // 
            this.LblID.AutoSize = true;
            this.LblID.Location = new System.Drawing.Point(79, 27);
            this.LblID.Name = "LblID";
            this.LblID.Size = new System.Drawing.Size(31, 17);
            this.LblID.TabIndex = 10;
            this.LblID.Text = "N/A";
            // 
            // TbName
            // 
            this.TbName.Location = new System.Drawing.Point(82, 55);
            this.TbName.Name = "TbName";
            this.TbName.Size = new System.Drawing.Size(260, 25);
            this.TbName.TabIndex = 11;
            // 
            // BtnReset
            // 
            this.BtnReset.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnReset.Location = new System.Drawing.Point(781, 117);
            this.BtnReset.Name = "BtnReset";
            this.BtnReset.Size = new System.Drawing.Size(75, 25);
            this.BtnReset.TabIndex = 19;
            this.BtnReset.Text = "Reset";
            this.BtnReset.UseVisualStyleBackColor = true;
            this.BtnReset.Click += new System.EventHandler(this.BtnReset_Click);
            // 
            // TbAddress
            // 
            this.TbAddress.Location = new System.Drawing.Point(82, 86);
            this.TbAddress.Name = "TbAddress";
            this.TbAddress.Size = new System.Drawing.Size(260, 25);
            this.TbAddress.TabIndex = 12;
            // 
            // BtnDelete
            // 
            this.BtnDelete.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnDelete.Location = new System.Drawing.Point(781, 86);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(75, 25);
            this.BtnDelete.TabIndex = 18;
            this.BtnDelete.Text = "Delete";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_ClickAsync);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(50, 27);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(23, 17);
            this.label21.TabIndex = 0;
            this.label21.Text = "ID:";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // BtnUpdate
            // 
            this.BtnUpdate.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnUpdate.Location = new System.Drawing.Point(781, 55);
            this.BtnUpdate.Name = "BtnUpdate";
            this.BtnUpdate.Size = new System.Drawing.Size(75, 25);
            this.BtnUpdate.TabIndex = 17;
            this.BtnUpdate.Text = "Update";
            this.BtnUpdate.UseVisualStyleBackColor = true;
            this.BtnUpdate.Click += new System.EventHandler(this.BtnUpdate_ClickAsync);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(22, 58);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(51, 17);
            this.label22.TabIndex = 1;
            this.label22.Text = "*Name:";
            // 
            // BtnSave
            // 
            this.BtnSave.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnSave.Location = new System.Drawing.Point(781, 24);
            this.BtnSave.Name = "BtnSave";
            this.BtnSave.Size = new System.Drawing.Size(75, 25);
            this.BtnSave.TabIndex = 16;
            this.BtnSave.Text = "Save";
            this.BtnSave.UseVisualStyleBackColor = true;
            this.BtnSave.Click += new System.EventHandler(this.BtnSave_ClickAsync);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(14, 89);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(59, 17);
            this.label23.TabIndex = 2;
            this.label23.Text = "Address:";
            // 
            // TbPhone
            // 
            this.TbPhone.Location = new System.Drawing.Point(497, 24);
            this.TbPhone.Name = "TbPhone";
            this.TbPhone.Size = new System.Drawing.Size(260, 25);
            this.TbPhone.TabIndex = 13;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(387, 27);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(104, 17);
            this.label26.TabIndex = 5;
            this.label26.Text = "*Phone Number:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(417, 89);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(74, 17);
            this.label28.TabIndex = 7;
            this.label28.Text = "Select type:";
            // 
            // ComboType
            // 
            this.ComboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboType.FormattingEnabled = true;
            this.ComboType.Items.AddRange(new object[] {
            "Customer",
            "Maintenance Vendor",
            "Vendor",
            "Subcontractor",
            "Supplier",
            "Employee",
            "Other"});
            this.ComboType.Location = new System.Drawing.Point(497, 86);
            this.ComboType.Name = "ComboType";
            this.ComboType.Size = new System.Drawing.Size(260, 25);
            this.ComboType.TabIndex = 15;
            // 
            // TbEmail
            // 
            this.TbEmail.Location = new System.Drawing.Point(497, 55);
            this.TbEmail.Name = "TbEmail";
            this.TbEmail.Size = new System.Drawing.Size(260, 25);
            this.TbEmail.TabIndex = 14;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(397, 58);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 17);
            this.label27.TabIndex = 6;
            this.label27.Text = "Email Address:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ListClient);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 249);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(876, 252);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Client list";
            // 
            // ListClient
            // 
            this.ListClient.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13});
            this.ListClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListClient.FullRowSelect = true;
            this.ListClient.Location = new System.Drawing.Point(3, 21);
            this.ListClient.Name = "ListClient";
            this.ListClient.Size = new System.Drawing.Size(870, 228);
            this.ListClient.TabIndex = 20;
            this.ListClient.UseCompatibleStateImageBehavior = false;
            this.ListClient.View = System.Windows.Forms.View.Details;
            this.ListClient.Click += new System.EventHandler(this.ListClient_Click);
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "ID";
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Name";
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Address";
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Phone";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Email";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Type";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.SteelBlue;
            this.panel6.Controls.Add(this.label20);
            this.panel6.Controls.Add(this.label19);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(876, 88);
            this.panel6.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(298, 48);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(280, 17);
            this.label20.TabIndex = 1;
            this.label20.Text = "Create, view or update general client database";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(336, 11);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(204, 37);
            this.label19.TabIndex = 0;
            this.label19.Text = "Client Database";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(884, 531);
            this.Controls.Add(this.TabControlMain);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FormMain";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_LoadAsync);
            this.TabControlMain.ResumeLayout(false);
            this.tabPageSearch.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabPagePurchase.ResumeLayout(false);
            this.tabPagePurchase.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPageSales.ResumeLayout(false);
            this.tabPageSales.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPageClientData.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControlMain;
        private System.Windows.Forms.TabPage tabPagePurchase;
        private System.Windows.Forms.TabPage tabPageSales;
        private System.Windows.Forms.TabPage tabPageSearch;
        private System.Windows.Forms.DateTimePicker datePicker;
        private System.Windows.Forms.TextBox tbQty;
        private System.Windows.Forms.TextBox tbPrice;
        private System.Windows.Forms.TextBox tbProductName;
        private System.Windows.Forms.Button btnPurchase;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox tbLeadTime;
        private System.Windows.Forms.TextBox tbQtySale;
        private System.Windows.Forms.TextBox tbPriceSale;
        private System.Windows.Forms.DateTimePicker datePickerSale;
        private System.Windows.Forms.Button btnClearSale;
        private System.Windows.Forms.Button btnSupply;
        private System.Windows.Forms.ComboBox comboProductName;
        private System.Windows.Forms.TabPage tabPageClientData;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button BtnRefresh;
        private System.Windows.Forms.Label LblProductName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TbSearch;
        private System.Windows.Forms.Button BtnSearch;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ListView ListSearch;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label LblID;
        private System.Windows.Forms.TextBox TbName;
        private System.Windows.Forms.TextBox TbAddress;
        private System.Windows.Forms.TextBox TbPhone;
        private System.Windows.Forms.TextBox TbEmail;
        private System.Windows.Forms.ComboBox ComboType;
        private System.Windows.Forms.Button BtnReset;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.Button BtnUpdate;
        private System.Windows.Forms.Button BtnSave;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView ListClient;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
    }
}

