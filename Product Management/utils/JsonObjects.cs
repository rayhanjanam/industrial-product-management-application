﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Management_Program.utils
{

    public class Product
    {
        public int id { get; set; }
        public string batch { get; set; }
        public string productName { get; set; }
        public int unit { get; set; }
        public string price { get; set; }
        public string date { get; set; }
        public string branch { get; set; }
        public int rol { get; set; }
    }

    public class ResponseMessage
    {
        public int code { get; set; }
        public string message { get; set; }
        public List<Product> data { get; set; }
    }


    public class ClientResponseMessage
    {
        public int code { get; set; }
        public string message { get; set; }
        public List<Client> data { get; set; }
    }

    public class Client
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string type { get; set; }
    }
}

