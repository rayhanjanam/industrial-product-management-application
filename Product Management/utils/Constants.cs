﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Management_Program.utils
{
    public static class Constants
    {
        public static readonly string LoginURL = "http://localhost/Inventory%20Management%20Server/api_login.php";
        public static readonly string ServerURL = "http://localhost/Inventory%20Management%20Server/api_server.php?mode=";

        public static readonly string PurchaseExtension = "purchase";
        public static readonly string ListExtension = "list";
        public static readonly string SalesExtension = "sales";
        public static readonly string SearchExtension = "search";
        public static readonly string RolExtension = "setrol";

    }
}
