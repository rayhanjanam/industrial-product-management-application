﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Inventory_Management_Program.utils
{
    class NetUtility
    {
        public static bool HasConnectionToServer()
        {
            try
            {
                TcpClient tcpClient = new TcpClient();
                tcpClient.Connect("localhost", 80);

                return true;
            }
            catch (Exception)
            {
            }

            return false;
        }
    }
}
