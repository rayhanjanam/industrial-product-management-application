﻿namespace Inventory_Management_Program
{
    partial class FormMonthlyView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tb6 = new System.Windows.Forms.TextBox();
            this.Tb5 = new System.Windows.Forms.TextBox();
            this.Tb4 = new System.Windows.Forms.TextBox();
            this.Tb3 = new System.Windows.Forms.TextBox();
            this.Tb2 = new System.Windows.Forms.TextBox();
            this.Tb1 = new System.Windows.Forms.TextBox();
            this.TbEOQ = new System.Windows.Forms.TextBox();
            this.TbLeadTime = new System.Windows.Forms.TextBox();
            this.TbService = new System.Windows.Forms.TextBox();
            this.TbSafetyStock = new System.Windows.Forms.TextBox();
            this.TbAnnualDemand = new System.Windows.Forms.TextBox();
            this.TbCost = new System.Windows.Forms.TextBox();
            this.TbHoldingCost = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnEOQ = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ComboMonth = new System.Windows.Forms.ComboBox();
            this.BtnROL = new System.Windows.Forms.Button();
            this.BtnSS = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tb6
            // 
            this.Tb6.Location = new System.Drawing.Point(450, 48);
            this.Tb6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Tb6.Name = "Tb6";
            this.Tb6.Size = new System.Drawing.Size(76, 25);
            this.Tb6.TabIndex = 5;
            // 
            // Tb5
            // 
            this.Tb5.Location = new System.Drawing.Point(364, 48);
            this.Tb5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Tb5.Name = "Tb5";
            this.Tb5.Size = new System.Drawing.Size(76, 25);
            this.Tb5.TabIndex = 4;
            // 
            // Tb4
            // 
            this.Tb4.Location = new System.Drawing.Point(278, 48);
            this.Tb4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Tb4.Name = "Tb4";
            this.Tb4.Size = new System.Drawing.Size(76, 25);
            this.Tb4.TabIndex = 3;
            // 
            // Tb3
            // 
            this.Tb3.Location = new System.Drawing.Point(192, 48);
            this.Tb3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Tb3.Name = "Tb3";
            this.Tb3.Size = new System.Drawing.Size(76, 25);
            this.Tb3.TabIndex = 2;
            // 
            // Tb2
            // 
            this.Tb2.Location = new System.Drawing.Point(106, 48);
            this.Tb2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Tb2.Name = "Tb2";
            this.Tb2.Size = new System.Drawing.Size(76, 25);
            this.Tb2.TabIndex = 1;
            // 
            // Tb1
            // 
            this.Tb1.Location = new System.Drawing.Point(20, 48);
            this.Tb1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Tb1.Name = "Tb1";
            this.Tb1.Size = new System.Drawing.Size(76, 25);
            this.Tb1.TabIndex = 0;
            // 
            // TbEOQ
            // 
            this.TbEOQ.Location = new System.Drawing.Point(177, 141);
            this.TbEOQ.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TbEOQ.Name = "TbEOQ";
            this.TbEOQ.Size = new System.Drawing.Size(182, 25);
            this.TbEOQ.TabIndex = 15;
            // 
            // TbLeadTime
            // 
            this.TbLeadTime.Location = new System.Drawing.Point(185, 71);
            this.TbLeadTime.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TbLeadTime.Name = "TbLeadTime";
            this.TbLeadTime.Size = new System.Drawing.Size(116, 25);
            this.TbLeadTime.TabIndex = 7;
            // 
            // TbService
            // 
            this.TbService.Location = new System.Drawing.Point(185, 106);
            this.TbService.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TbService.Name = "TbService";
            this.TbService.Size = new System.Drawing.Size(116, 25);
            this.TbService.TabIndex = 8;
            // 
            // TbSafetyStock
            // 
            this.TbSafetyStock.Location = new System.Drawing.Point(185, 141);
            this.TbSafetyStock.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TbSafetyStock.Name = "TbSafetyStock";
            this.TbSafetyStock.Size = new System.Drawing.Size(182, 25);
            this.TbSafetyStock.TabIndex = 9;
            // 
            // TbAnnualDemand
            // 
            this.TbAnnualDemand.Location = new System.Drawing.Point(177, 36);
            this.TbAnnualDemand.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TbAnnualDemand.Name = "TbAnnualDemand";
            this.TbAnnualDemand.Size = new System.Drawing.Size(116, 25);
            this.TbAnnualDemand.TabIndex = 12;
            // 
            // TbCost
            // 
            this.TbCost.Location = new System.Drawing.Point(177, 71);
            this.TbCost.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TbCost.Name = "TbCost";
            this.TbCost.Size = new System.Drawing.Size(116, 25);
            this.TbCost.TabIndex = 13;
            // 
            // TbHoldingCost
            // 
            this.TbHoldingCost.Location = new System.Drawing.Point(177, 106);
            this.TbHoldingCost.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.TbHoldingCost.Name = "TbHoldingCost";
            this.TbHoldingCost.Size = new System.Drawing.Size(116, 25);
            this.TbHoldingCost.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 17);
            this.label1.TabIndex = 14;
            this.label1.Text = "Month 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 17);
            this.label2.TabIndex = 15;
            this.label2.Text = "Month 2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(201, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 17);
            this.label3.TabIndex = 16;
            this.label3.Text = "Month 3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(286, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 17);
            this.label4.TabIndex = 17;
            this.label4.Text = "Month 4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(371, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Month 5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(456, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(57, 17);
            this.label6.TabIndex = 19;
            this.label6.Text = "Month 6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(149, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Demand Period (Month)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(48, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 17);
            this.label8.TabIndex = 21;
            this.label8.Text = "Lead Time (Month)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(61, 109);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(105, 17);
            this.label9.TabIndex = 22;
            this.label9.Text = "Service Level (%)";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(58, 39);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 17);
            this.label10.TabIndex = 23;
            this.label10.Text = "Annual Demand";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(88, 144);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 17);
            this.label11.TabIndex = 24;
            this.label11.Text = "Safety Stock";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(28, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 17);
            this.label12.TabIndex = 25;
            this.label12.Text = "Cost Per Order (BDT)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(40, 109);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(118, 17);
            this.label13.TabIndex = 26;
            this.label13.Text = "Holding Cost (BDT)";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(123, 144);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(35, 17);
            this.label14.TabIndex = 27;
            this.label14.Text = "EOQ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.BtnEOQ);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.TbEOQ);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.TbAnnualDemand);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.TbCost);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.TbHoldingCost);
            this.groupBox1.Location = new System.Drawing.Point(20, 335);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(506, 222);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "EOQ";
            // 
            // BtnEOQ
            // 
            this.BtnEOQ.Location = new System.Drawing.Point(177, 181);
            this.BtnEOQ.Name = "BtnEOQ";
            this.BtnEOQ.Size = new System.Drawing.Size(182, 27);
            this.BtnEOQ.TabIndex = 16;
            this.BtnEOQ.Text = "Calculate EOQ";
            this.BtnEOQ.UseVisualStyleBackColor = true;
            this.BtnEOQ.Click += new System.EventHandler(this.BtnEOQ_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ComboMonth);
            this.groupBox2.Controls.Add(this.BtnROL);
            this.groupBox2.Controls.Add(this.BtnSS);
            this.groupBox2.Controls.Add(this.TbLeadTime);
            this.groupBox2.Controls.Add(this.TbService);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.TbSafetyStock);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Location = new System.Drawing.Point(20, 92);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(506, 222);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Safety Stock and ROL";
            // 
            // ComboMonth
            // 
            this.ComboMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboMonth.FormattingEnabled = true;
            this.ComboMonth.Items.AddRange(new object[] {
            "Month 1",
            "Month 2",
            "Month 3",
            "Month 4",
            "Month 5",
            "Month 6"});
            this.ComboMonth.Location = new System.Drawing.Point(185, 36);
            this.ComboMonth.Name = "ComboMonth";
            this.ComboMonth.Size = new System.Drawing.Size(116, 25);
            this.ComboMonth.TabIndex = 25;
            this.ComboMonth.SelectedIndexChanged += new System.EventHandler(this.ComboMonth_SelectedIndexChanged);
            // 
            // BtnROL
            // 
            this.BtnROL.Location = new System.Drawing.Point(418, 181);
            this.BtnROL.Name = "BtnROL";
            this.BtnROL.Size = new System.Drawing.Size(75, 27);
            this.BtnROL.TabIndex = 11;
            this.BtnROL.Text = "Set ROL";
            this.BtnROL.UseVisualStyleBackColor = true;
            this.BtnROL.Click += new System.EventHandler(this.BtnROL_ClickAsync);
            // 
            // BtnSS
            // 
            this.BtnSS.Location = new System.Drawing.Point(187, 181);
            this.BtnSS.Name = "BtnSS";
            this.BtnSS.Size = new System.Drawing.Size(182, 27);
            this.BtnSS.TabIndex = 10;
            this.BtnSS.Text = "Calculate Safety Stock";
            this.BtnSS.UseVisualStyleBackColor = true;
            this.BtnSS.Click += new System.EventHandler(this.BtnSS_Click);
            // 
            // FormMonthlyView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 17F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(543, 573);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Tb1);
            this.Controls.Add(this.Tb2);
            this.Controls.Add(this.Tb3);
            this.Controls.Add(this.Tb4);
            this.Controls.Add(this.Tb5);
            this.Controls.Add(this.Tb6);
            this.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.Name = "FormMonthlyView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Tb6;
        private System.Windows.Forms.TextBox Tb5;
        private System.Windows.Forms.TextBox Tb4;
        private System.Windows.Forms.TextBox Tb3;
        private System.Windows.Forms.TextBox Tb2;
        private System.Windows.Forms.TextBox Tb1;
        private System.Windows.Forms.TextBox TbEOQ;
        private System.Windows.Forms.TextBox TbLeadTime;
        private System.Windows.Forms.TextBox TbService;
        private System.Windows.Forms.TextBox TbSafetyStock;
        private System.Windows.Forms.TextBox TbAnnualDemand;
        private System.Windows.Forms.TextBox TbCost;
        private System.Windows.Forms.TextBox TbHoldingCost;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button BtnEOQ;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button BtnSS;
        private System.Windows.Forms.Button BtnROL;
        private System.Windows.Forms.ComboBox ComboMonth;
    }
}