﻿using System;
using System.Net.Http;
using System.Windows.Forms;

using Newtonsoft.Json;
using Inventory_Management_Program.utils;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Inventory_Management_Program
{
    public partial class FormMain : Form
    {
        private static readonly HttpClient httpClient = new HttpClient();

        private string branchName;

        #region Forms

        public FormMain()
        {
            InitializeComponent();

            Text = Application.ProductName;
            FormBorderStyle = FormBorderStyle.Fixed3D;
        }

        public FormMain(string branch)
        {
            InitializeComponent();

            Text = Application.ProductName;
            FormBorderStyle = FormBorderStyle.Fixed3D;

            branchName = branch;
        }

        private async void FormMain_LoadAsync(object sender, EventArgs e)
        {
            await LoadPurchaseData();
            await LoadClientData();
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show(
                this, "You will be logged out. Are you sure?",
                "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);

            if (dialogResult == DialogResult.OK)
            {
                Application.ExitThread();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tbProductName.Clear();
            tbPrice.Clear();
            tbQty.Clear();
            LblProductName.Text = "Not selected";

            datePicker.ResetText();
        }

        #endregion

        #region Purchase

        private async void btnPurchase_ClickAsync(object sender, EventArgs e)
        {
            string productName, productPrice, qty;

            productName = tbProductName.Text;
            productPrice = tbPrice.Text;
            qty = tbQty.Text;

            if (productName.Length > 0 && productPrice.Length > 0 && qty.Length > 0)
            {
                double price = 0.0;
                int quantity = 0;

                try
                {
                    price = double.Parse(productPrice);
                    quantity = int.Parse(qty);
                }
                catch (Exception)
                {
                    MessageBox.Show("Please insert valid data",
                        Application.ProductName + ": Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                finally
                {
                    if (price > 0.0 && quantity > 0)
                    {
                        // entry to database

                        btnPurchase.Enabled = false;

                        var parameters = new Dictionary<string, string>
                        {
                            {"name", productName},
                            {"unit", quantity.ToString()},
                            {"price", price.ToString()},
                            {"date", datePicker.Text},
                            {"branch", branchName}
                        };

                        var content = new FormUrlEncodedContent(parameters);
                        var response = await httpClient.PostAsync(
                            Constants.ServerURL + Constants.PurchaseExtension, content);
                        var responseText = await response.Content.ReadAsStringAsync();

                        btnPurchase.Enabled = true;

                        var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseText);

                        if (msg != null)
                        {
                            if (msg.code == 100)
                            {
                                var result = MessageBox.Show("Purchase Success. Open demand forecast?",
                                        Application.ProductName,
                                        MessageBoxButtons.OKCancel,
                                        MessageBoxIcon.Information);

                                if (result == DialogResult.OK)
                                {
                                    // goto monthly view
                                    new FormMonthlyView(msg.message).ShowDialog(this);
                                }
                            }
                            else
                                MessageBox.Show(msg.message,
                                        Application.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                        }


                    }
                    else
                    {
                        MessageBox.Show("Invalid data",
                            Application.ProductName + ": Error",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }
                }

            }
            else
            {
                MessageBox.Show("Required field/s is/are left empty",
                    Application.ProductName + ": Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private async Task LoadPurchaseData()
        {
            var response = await httpClient.GetAsync(Constants.ServerURL + Constants.ListExtension);
            var responseString = await response.Content.ReadAsStringAsync();

            var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseString);

            if (msg != null)
            {
                if (msg.code == 10)
                {
                    if (msg.data != null)
                    {
                        var purchaseData = msg.data;
                        comboProductName.Items.Clear();

                        foreach (Product item in purchaseData)
                        {
                            string comboItem = item.productName + " | " + item.batch + " | x" + item.unit;
                            comboProductName.Items.Add(comboItem);
                        }
                        comboProductName.Refresh();
                    }
                }
            }
        }

        private async void BtnRefresh_ClickAsync(object sender, EventArgs e)
        {
            await LoadPurchaseData();
        }

        #endregion

        #region Sales

        private async void btnSupply_ClickAsync(object sender, EventArgs e)
        {
            string productName = comboProductName.Text;
            string productPrice = tbPriceSale.Text;
            string productQuantity = tbQtySale.Text;
            string productLeadTime = tbLeadTime.Text;
            string date = datePickerSale.Text;

            if (productName.Length > 0 && productPrice.Length > 0 &&
                productQuantity.Length > 0 && productLeadTime.Length > 0)
            {
                double price = 0.0;
                double leadTime = 0.0;
                int quantity = 0;

                try
                {
                    price = double.Parse(productPrice);
                    leadTime = double.Parse(productLeadTime);
                    quantity = int.Parse(productQuantity);
                }
                catch (Exception)
                {
                    MessageBox.Show("Please insert valid data",
                        Application.ProductName + ": Error",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                finally
                {
                    if (price > 0.0 && leadTime > 0.0 && quantity > 0)
                    {
                        // entry to database

                        string batchNumber = getBatchNumber(comboProductName.SelectedItem.ToString());

                        if (batchNumber != null)
                        {
                            var parameters = new Dictionary<string, string>
                            {
                                {"batch", batchNumber},
                                {"unit", quantity.ToString()},
                                {"price", price.ToString()},
                                {"leadtime", leadTime.ToString()},
                                {"date", datePickerSale.Text}
                            };

                            var content = new FormUrlEncodedContent(parameters);
                            var response = await httpClient.PostAsync(
                                Constants.ServerURL + Constants.SalesExtension, content);
                            var responseString = await response.Content.ReadAsStringAsync();

                            var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseString);

                            if (msg != null)
                            {
                                if (msg.code == 1000)
                                {
                                    MessageBox.Show(msg.message,
                                        Application.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                }
                                else if (msg.code == 1003)
                                {
                                    MessageBox.Show(msg.message,
                                        Application.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                                }
                                else
                                {
                                    MessageBox.Show(msg.message,
                                       Application.ProductName,
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Information);
                                }
                            }
                            else
                                MessageBox.Show("Error getting data",
                                            Application.ProductName,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                        }
                        else
                            MessageBox.Show("Incorrect batch number",
                                            Application.ProductName,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Required field/s is/are left empty",
                    Application.ProductName + ": Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private string getBatchNumber(string item)
        {
            if (item.Contains("|"))
            {
                string[] s = item.Trim().Split('|');

                if (s[1].Length > 0)
                {
                    return s[1].Trim();
                }
            }

            return null;
        }

        private void comboProductName_SelectedIndexChanged(object sender, EventArgs e)
        {
            LblProductName.Text = comboProductName.Text.Split('|')[0].Trim();
        }

        private void btnClearSale_Click(object sender, EventArgs e)
        {
            comboProductName.ResetText();
            tbPriceSale.Clear();
            tbQtySale.Clear();
            tbLeadTime.Clear();
            datePickerSale.ResetText();

            LblProductName.Text = "Not selected";
        }

        #endregion

        #region Search

        private void ListSearch_DoubleClick(object sender, EventArgs e)
        {
            new FormMonthlyView(ListSearch.SelectedItems[0].SubItems[2].Text).ShowDialog();
        }

        private async void BtnSearch_ClickAsync(object sender, EventArgs e)
        {
            ListSearch.Items.Clear();

            string query = TbSearch.Text;

            if (query.Length > 0)
            {
                var content = new FormUrlEncodedContent(new Dictionary<string, string>
                {
                    { "query", query }
                });
                var response = await httpClient.PostAsync(Constants.ServerURL + Constants.SearchExtension, content);
                var responseString = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseString);

                if (msg != null)
                {
                    if (msg.code == 10)
                    {
                        if (msg.data != null)
                        {
                            List<Product> data = msg.data;
                            foreach (var item in data)
                            {
                                var listItem = new ListViewItem(new string[]
                                {
                                    item.id.ToString(), item.productName,
                                    item.batch, item.unit.ToString(), item.price,
                                    item.date, item.branch
                                });

                                ListSearch.Items.Add(listItem);
                            }


                            foreach (ColumnHeader item in ListSearch.Columns)
                            {
                                item.Width = -2;
                            }

                        }
                    }
                }
            }
        }


        #endregion

        #region Client

        private async Task LoadClientData()
        {
            ListClient.Items.Clear();

            var response = await httpClient.GetAsync(Constants.ServerURL + "clientlist");
            var responseString = await response.Content.ReadAsStringAsync();

            var msg = JsonConvert.DeserializeObject<ClientResponseMessage>(responseString);

            if (msg != null)
            {
                if (msg.code == 10)
                {
                    if (msg.data != null)
                    {
                        List<Client> data = msg.data;
                        foreach (var item in data)
                        {
                            var listItem = new ListViewItem(new string[]
                            {
                                    item.id.ToString(), item.name,
                                    item.address, item.phone, item.email,
                                    item.type
                            });

                            ListClient.Items.Add(listItem);
                        }


                        foreach (ColumnHeader item in ListClient.Columns)
                        {
                            item.Width = -2;
                        }

                    }
                }
            }
        }


        private async void BtnSave_ClickAsync(object sender, EventArgs e)
        {
            string name, address, phone, email;

            name = TbName.Text;
            address = TbAddress.Text;
            phone = TbPhone.Text;
            email = TbEmail.Text;

            if (ComboType.Text.Length > 0 && name.Length > 0 && phone.Length > 0)
            {
                var values = new Dictionary<string, string>
                {
                    {"name", name},
                    {"address", address},
                    {"phone", phone },
                    {"email", email },
                    {"type", ComboType.SelectedItem.ToString() }
                };

                var content = new FormUrlEncodedContent(values);
                var response = await httpClient.PostAsync(Constants.ServerURL + "clientadd", content);
                var responseString = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<ClientResponseMessage>(responseString);

                if (msg.code == 10)
                {
                    MessageBox.Show(this, msg.message, "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    await LoadClientData();

                }
            }
        }

        private async void BtnUpdate_ClickAsync(object sender, EventArgs e)
        {
            string id, name, address, phone, email;

            id = LblID.Text;
            name = TbName.Text;
            address = TbAddress.Text;
            phone = TbPhone.Text;
            email = TbEmail.Text;

            if (ComboType.SelectedIndex >= 0 && name.Length > 0 && phone.Length > 0 && id.Length > 0 && id != "N/A")
            {
                var values = new Dictionary<string, string>
                {
                    {"id", id },
                    {"name", name},
                    {"address", address},
                    {"phone", phone },
                    {"email", email },
                    {"type", ComboType.SelectedItem.ToString() }
                };

                var content = new FormUrlEncodedContent(values);
                var response = await httpClient.PostAsync(Constants.ServerURL + "clientupdate", content);
                var responseString = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseString);

                if (msg.code == 10)
                {
                    MessageBox.Show(this, msg.message, "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    await LoadClientData();
                }
            }
        }

        private async void BtnDelete_ClickAsync(object sender, EventArgs e)
        {
            string id;

            id = LblID.Text;

            if (id.Length > 0 && id != "N/A")
            {
                var values = new Dictionary<string, string>
                {
                    {"id", id},
                };

                var content = new FormUrlEncodedContent(values);
                var response = await httpClient.PostAsync(Constants.ServerURL + "clientdelete", content);
                var responseString = await response.Content.ReadAsStringAsync();

                var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseString);

                if (msg.code == 10)
                {
                    MessageBox.Show(this, msg.message, "Notice", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    await LoadClientData();
                }
            }
        }

        private void BtnReset_Click(object sender, EventArgs e)
        {
            LblID.ResetText();
            TbName.Clear();
            TbAddress.Clear();
            TbPhone.Clear();
            TbEmail.Clear();
            ComboType.SelectedIndex = -1;
        }

        private void ListClient_Click(object sender, EventArgs e)
        {
            LblID.Text = ListClient.SelectedItems[0].SubItems[0].Text;
            TbName.Text = ListClient.SelectedItems[0].SubItems[1].Text;
            TbAddress.Text = ListClient.SelectedItems[0].SubItems[2].Text;
            TbPhone.Text = ListClient.SelectedItems[0].SubItems[3].Text;
            TbEmail.Text = ListClient.SelectedItems[0].SubItems[4].Text;

            ComboType.SelectedIndex = ComboType.Items.IndexOf(ListClient.SelectedItems[0].SubItems[5].Text);
        }
    }

    #endregion

}
