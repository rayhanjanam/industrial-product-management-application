﻿using Inventory_Management_Program.utils;
using MathNet.Numerics.Distributions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inventory_Management_Program
{
    public partial class FormMonthlyView : Form
    {
        private static readonly HttpClient httpClient = new HttpClient();

        private string batchNumber;
        private List<TextBox> months;
        private TextBox tbInEffect;

        public FormMonthlyView(string batch)
        {
            InitializeComponent();

            months = new List<TextBox> { Tb1, Tb2, Tb3, Tb4, Tb5, Tb6 };

            batchNumber = batch;
            Text = Application.ProductName + " - Demand Forecast";
        }

        private void BtnSS_Click(object sender, EventArgs e)
        {
            if (tbInEffect != null)
            {
                string monthlyDemandText = tbInEffect.Text;
                string leadTimeText = TbLeadTime.Text;
                string serviceText = TbService.Text;

                if (monthlyDemandText.Length > 0 && leadTimeText.Length > 0 && serviceText.Length > 0)
                {
                    double monthlyDemand = 0.0, service = 0.0;
                    double leadTime = 0;

                    try
                    {
                        monthlyDemand = double.Parse(monthlyDemandText);
                        leadTime = double.Parse(leadTimeText);
                        service = double.Parse(serviceText);
                    }
                    catch (Exception x)
                    {
                        MessageBox.Show(x.Message,
                                        Application.ProductName,
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                    finally
                    {
                        double serviceValue = new Normal().InverseCumulativeDistribution(service);

                        double safetyStock = (monthlyDemand * leadTime * serviceValue) + 1;

                        TbSafetyStock.Text = Convert.ToInt32(Math.Round(safetyStock, MidpointRounding.AwayFromZero)).ToString();
                    }
                }
            }
        }

        private void ComboMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            tbInEffect = months.ElementAt(ComboMonth.SelectedIndex);
        }

        private async void BtnROL_ClickAsync(object sender, EventArgs e)
        {
            try
            {
                double sum = 0;

                foreach (var item in months)
                {
                    sum += Convert.ToDouble(item.Text);
                }

                double demand = sum / 6;

                if (int.TryParse(TbSafetyStock.Text, out int stock) && double.TryParse(TbLeadTime.Text, out double leadTime))
                {
                    double rol = demand * leadTime + stock;

                    var parameters = new Dictionary<string, string>
                    {
                        {"batch", batchNumber},
                        {"rol", Convert.ToInt32(Math.Round(rol, MidpointRounding.AwayFromZero)).ToString()}
                    };

                    var content = new FormUrlEncodedContent(parameters);
                    var response = await httpClient.PostAsync(Constants.ServerURL + Constants.RolExtension, content);
                    var responseString = await response.Content.ReadAsStringAsync();

                    var msg = JsonConvert.DeserializeObject<ResponseMessage>(responseString);

                    if (msg != null)
                    {
                        if (msg.code == 10000)
                        {
                            MessageBox.Show(msg.message,
                                            Application.ProductName,
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message,
                                                       Application.ProductName,
                                                       MessageBoxButtons.OK,
                                                       MessageBoxIcon.Error);
            }


        }

        private void BtnEOQ_Click(object sender, EventArgs e)
        {
            double annualDemand, costPerOrder, holdingCost;

            try
            {
                annualDemand = double.Parse(TbAnnualDemand.Text);
                costPerOrder = double.Parse(TbCost.Text);
                holdingCost = double.Parse(TbHoldingCost.Text);

                double eoq = 0.0;

                eoq = Math.Sqrt(2 * annualDemand * costPerOrder / holdingCost);
                TbEOQ.Text = Math.Round(eoq, 2, MidpointRounding.AwayFromZero).ToString();
            }
            catch (Exception x)
            {
                MessageBox.Show(x.Message,
                                       Application.ProductName,
                                       MessageBoxButtons.OK,
                                       MessageBoxIcon.Error);
            }
        }
    }
}
